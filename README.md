# LoRaWAN Range Mapper_v1.0
When deploying a LoRaWAN network you want to know your gateway coverage and measure the radio performance in different places. A field tester is a simple, mobile, tool to help you decide the best location and monitor the coverage in a zone. When deploying a Chirpstack network is developed and the GPS coordinates are mapped for visualization. Then you want to enrich the network mappers to know and share the real network coverage.

![image1](https://gitlab.com/jaison_j/lorawan-range-mapper_v1.0/-/raw/master/Images/IMG_1.jpg)*Range_Mapper_v1.0*
# Features

   - Easy selection for Power / Sf / max retries
   - Duty cycle status display
   - Last setting flash memory backup
   - Graph selection display
        - Rx Rssi - acked Rssi level
        - Rx Snr - acked Snr level
        - Retries - number of retry before getting a ack
        - Tx Rssi - network side Rssi
        - Hotspots - network side hotspot
        - Distance - hotspot distance (in progress)
        - GPS status - current GPS status
   - Manual / Automatic mode switch
   - Display last frame information
   - Network side RSSI / SNR / #of station
   - Your favourite network splash screen (select in config.h)
   - Screen shot and user guide
   - RFM95 Schematics
   - GPS position reporting with Chirpstack mapper integration
   - LiPo charging mode detection
   - PCB available
   - Enclosure available
   - Serial port LoRaWan setup configuration
   - End-user setup screen
   - Ready for industrial production with credential commissioning
   - Support firmware upgrade w/o credential loss (LoRa-E5 only)
   - Reduce uplink messages when device is not moving / out of coverage


# Getting Started

- Make sure that you have a [Wio Terminal](https://wiki.seeedstudio.com/Wio-Terminal-Getting-Started/) and [Wio Terminal LoRaWAN Chassis](https://www.seeedstudio.com/Wio-Terminal-Chassis-LoRa-E5-and-GNSS-p-5053.html)
- Install Arduino IDE.
- Select board : Wio Terminal
- For connecting Wio Terminal to Arduino click [here](https://wiki.seeedstudio.com/Wio-Terminal-Getting-Started/)
- How to [setup your WioLoRaWanFieldTester device](https://gitlab.com/jaison_j/lorawan-range-mapper_v1.0/-/blob/master/Software/LoRaWAN%20Range%20Mapper_v1.0/doc/SETUP.md)
- [User Guide](https://gitlab.com/jaison_j/lorawan-range-mapper_v1.0/-/blob/master/Software/LoRaWAN%20Range%20Mapper_v1.0/doc/UserGuide.md)


# Prerequisites
  - Arduino IDE 1.8.19[Tested]
  - Python 3
  - Wio Terminal
  - Wio Terminal Battery Chassis
  - Wio Terminal LoRaWAN Chassis

# License
This project is licensed under the MIT License - see the LICENSE.md file for details

# Troubleshooting
GPS never fixing (even outdoor)
Some of the LoRa-E5 chassis have an incorrect default UART speed at 115200. To reset it to the normal value, flash the firmware [Gps_Reset_To_9600bps](https://gitlab.com/jaison_j/lorawan-range-mapper_v1.0/-/blob/master/Software/LoRaWAN%20Range%20Mapper_v1.0/GPS_Reset_binaries/Gps_Reset_To_9600bps.uf2). Then you wait for about 1 minute and then reflash the latest version. This should fix it.

# Acknowledgments
 - [disk 91](https://github.com/disk91/WioLoRaWANFieldTester)

 # Contribute to development
Read the [developer documentation](https://gitlab.com/jaison_j/lorawan-range-mapper_v1.0/-/blob/master/Software/LoRaWAN%20Range%20Mapper_v1.0/doc/DEVELOPMENT.md)
# Changelog
